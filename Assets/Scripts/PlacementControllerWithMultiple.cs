﻿using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.EventSystems;



[RequireComponent(typeof(ARRaycastManager))]
public class PlacementControllerWithMultiple : MonoBehaviour
{
    private string cableObjectName = "CableObject";
    
    [SerializeField]
    private Button cableButton;

    [SerializeField]
    private Button engineButton;

    [SerializeField]
    private Button dismissButton;

    [SerializeField]
    private Button resetButton;

    [SerializeField]
    private Button labelTogglerButton;

    public ARSession session;

    [SerializeField]
    private GameObject welcomePanel;

    [SerializeField]
    private GameObject optionsPanel;

    [SerializeField]
    private GameObject tooltipPanel;

    [SerializeField]
    private GameObject resetPanel;

    [SerializeField]
    private GameObject labelTogglerPanel;

    [SerializeField]
    public GameObject deconstructPanel;

    private GameObject labelObject;

    [SerializeField]
    private Text selectionText;

    private GameObject placedPrefab;

    private GameObject placedPrefabWithLabels;

    private ARRaycastManager arRaycastManager;

    private PMBIndicator pmbIndicator;

    private AssetHandlerManager assetHandlerManager; 

    public bool isDetecting;

    public bool labelActive = true;

    public bool deconstructFlag = false;

    private CableDeconstruct cableDeconstruct;

    public GameObject obj;

    public string selectedPrefabName = "";


    void Awake() 
    {
        arRaycastManager = GetComponent<ARRaycastManager>();

        // set initial prefab
        ChangePrefabTo(cableObjectName);
        cableButton.onClick.AddListener(() => ChangePrefabTo(cableObjectName));
        engineButton.onClick.AddListener(() => ChangePrefabTo("EngineObject"));
        dismissButton.onClick.AddListener(Dismiss);
        resetButton.onClick.AddListener(()=>{ 
            Destroy(obj);
            resetPanel.SetActive(false);
            labelTogglerPanel.SetActive(false);
            DetectionSwitch();
            optionsPanel.SetActive(true);
            tooltipPanel.SetActive(true);
            if(deconstructFlag)
            {
                if(selectedPrefabName != cableObjectName)
                    deconstructFlag = false;
                deconstructPanel.SetActive(false);
                cableDeconstruct.deconstructLevel = 0;
            }
                           
        });
        labelTogglerButton.onClick.AddListener(LabelToggle);
    }

    

    private void Dismiss() {
        welcomePanel.SetActive(false);
        tooltipPanel.SetActive(true);
        optionsPanel.SetActive(true);
    }

    private void LabelToggle() {
        if(labelActive)
        {
            labelActive = false;
            obj.transform.GetChild(1).gameObject.SetActive(false);
            // Destroy(obj);       
            // obj = Instantiate(placedPrefab, previouslyPlaced.transform.position, previouslyPlaced.transform.rotation);
            // obj.transform.localScale = previouslyPlaced.transform.localScale;
            // previouslyPlaced = obj;
            // Debug.LogWarning("Label Object deactivated");
        }
        else
        {
            labelActive = true;
            obj.transform.GetChild(1).gameObject.SetActive(true);
            // Destroy(obj);
            // obj = Instantiate(placedPrefabWithLabels, previouslyPlaced.transform.position, previouslyPlaced.transform.rotation);
            // obj.transform.localScale = previouslyPlaced.transform.localScale;
            // previouslyPlaced = obj; 
            // Debug.LogWarning("Label Object activated");        
        }
    }

    public void DetectionSwitch(){
        if(isDetecting)
        {
            isDetecting = false;
            pmbIndicator.visual.SetActive(false); 
        }
        //Now used to enable detection after destroying object          
        else
        {
            isDetecting = true;
            pmbIndicator.visual.SetActive(true);        
        }
            
    }


    void Start()
    {
        pmbIndicator = FindObjectOfType<PMBIndicator>();        
        cableDeconstruct = FindObjectOfType<CableDeconstruct>();
        assetHandlerManager = FindObjectOfType<AssetHandlerManager>();
        isDetecting = true;
        resetPanel.SetActive(false);
        labelTogglerPanel.SetActive(false);
        tooltipPanel.SetActive(false);
        optionsPanel.SetActive(false);
        deconstructPanel.SetActive(false);
    }

    void ChangePrefabTo(string prefabName)
    {
        placedPrefab = Resources.Load<GameObject>($"Prefabs/{prefabName}");

        if(placedPrefab == null)
            Debug.LogError($"Prefab with name {prefabName} could not be loaded, make sure you check the naming of your prefabs...");
        else
            selectedPrefabName = prefabName;
        
        switch(prefabName)
        {
            case "CableObject":
            {
                selectionText.text = $"Selected: {prefabName}";
                deconstructFlag = true;
            }     
            break;
            case "EngineObject":
            {
                selectionText.text = $"Selected: {prefabName}";
                deconstructFlag = false;
            }
            break;
            
        }
    }

    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public static event Action onPlacedObject;

    

    void Update()
    {
        if(placedPrefab == null || welcomePanel.gameObject.activeSelf)
        {
            return;
        }

        if((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began) && isDetecting && !IsPointerOverUIObject())
        {
            obj = Instantiate(placedPrefab, pmbIndicator.transform.position, pmbIndicator.transform.rotation);
            if(pmbIndicator.visual.activeInHierarchy)
            {
                DetectionSwitch();
                optionsPanel.SetActive(false);
                tooltipPanel.SetActive(false);
                resetPanel.SetActive(true);
                if(selectedPrefabName == cableObjectName)
                    labelTogglerPanel.SetActive(true);
                if(deconstructFlag)
                    deconstructPanel.SetActive(true);
            }

            // if (onPlacedObject != null)
            // {
            //     onPlacedObject();
            // }
            // commented because it seems like it only shows indicator on touch, which is unnecessary
            // if(!pmbIndicator.visual.activeInHierarchy)
            //     pmbIndicator.visual.SetActive(true);
            
        }

       
    }


}
