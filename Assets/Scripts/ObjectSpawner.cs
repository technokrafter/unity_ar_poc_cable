﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    private PMBIndicator pmbIndicator;
    private PlacementControllerWithMultiple placedPrefab;
    public bool isDetecting;
    
    // Start is called before the first frame update
    void Start()
    {
        pmbIndicator = FindObjectOfType<PMBIndicator>();
        placedPrefab = FindObjectOfType<PlacementControllerWithMultiple>();
        isDetecting = true;
        
    }

    public void DetectionOff(){
        if(isDetecting)
        {
            isDetecting = false;
            pmbIndicator.visual.SetActive(false);           
        }
        //Commented to prevent re-enabling plane detection, so as to only spawn one object           
        // else
        // {
        //     isDetecting = true;
        //     pmbIndicator.visual.SetActive(true);          
        // }
            
    }

    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    // Update is called once per frame
    void Update()
    {
        if((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began) && isDetecting && !IsPointerOverUIObject())
        {
            GameObject obj = Instantiate(objectToSpawn, pmbIndicator.transform.position, pmbIndicator.transform.rotation);
            DetectionOff();
            // commented because it seems like it only shows indicator on touch, which is unnecessary
            // if(!pmbIndicator.visual.activeInHierarchy)
            //     pmbIndicator.visual.SetActive(true);
            
        }
        
        


    }
}
