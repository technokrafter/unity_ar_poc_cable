﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using System.Text;


public class AssetHandlerManager : MonoBehaviour
{
    public AsyncOperationHandle<GameObject> stallObjectRemote;

    public AsyncOperationHandle<GameObject> spawnedObject;

    private AddrPMBIndicator pmbIndicator;

    private bool asyncObjectStatus;
    StringBuilder stringBuilder;

    // Start is called before the first frame update
    void Start()
    {
        pmbIndicator = FindObjectOfType<AddrPMBIndicator>();
        // LoadRemotePrefab();
    }

    public void LoadRemotePrefab()
    {
        Debug.Log("Entered LoadRemotePrefab");
        //Display remote prefabs
        // stallObjectRemote = Addressables.LoadAssetAsync<GameObject>("StallObject");
        // Debug.Log("Stall object loaded async");
        //Addressables.InstantiateAsync("StallObject", pmbIndicator.transform.position, pmbIndicator.transform.rotation).Completed += OnLoadDone;
        Addressables.LoadAssetAsync<GameObject>("StallObject").Completed += OnLoadDependencyDone;
        // Addressables.LoadAssetAsync<GameObject>("StallObject");
        // spawnedObject = Addressables.InstantiateAsync("StallObject", Vector3.zero, Quaternion.identity);

        // Addressables.InstantiateAsync(stallObjectRemote, pmbIndicator.transform.position, pmbIndicator.transform.rotation);
        
        // OnLoadDone(spawnedObject);
        // Addressables.InstantiateAsync("StallObject", pmbIndicator.transform.position, pmbIndicator.transform.rotation);

    }

    public void OnLoadDependencyDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded) {
            Addressables.InstantiateAsync(obj, pmbIndicator.transform.position, pmbIndicator.transform.rotation).Completed += OnLoadDone;
            Debug.Log("Addressable async load succeeded");
        }
        else
            Debug.LogError("Addressable async load failed");
    }

    
    private void OnLoadDone(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
    {
        if(obj.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("Addressable async instantiate succeeded");
            asyncObjectStatus = true;
        }
        
        else
        {
            Debug.LogError("Addressable async instantiate failed");
            asyncObjectStatus = false;
        }


        // stallObject = obj.Result;
    }


    // public void InstantiateRemotePrefab()
    // {
    //     // spawned = Addressables.InstantiateAsync("StallObject", Vector3.zero, Quaternion.identity);
    //     spawnedObject = Addressables.InstantiateAsync(stallObjectRemote, pmbIndicator.transform.position, pmbIndicator.transform.rotation);
    //     // Addressables.InstantiateAsync(stallObjectRemote, pmbIndicator.transform.position, pmbIndicator.transform.rotation);
    //     Debug.LogWarning("Stall object instantiated async");
    //     OnLoadDone(spawnedObject);
    //     // Addressables.InstantiateAsync("StallObject", pmbIndicator.transform.position, pmbIndicator.transform.rotation);
    // }


    public void DestroyRemotePrefab()
    {
        if(asyncObjectStatus)
        {
            Addressables.ReleaseInstance(spawnedObject);
            Debug.Log("Instance released and async object destroyed");
        }
            
        else
        {
            Debug.LogWarning("cannot destroy object as it hasn't instantiated");
            return;
        }
            
    }




    // Update is called once per frame
    void Update()
    {
        
    }
}
