﻿using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.EventSystems;

public class AddrPlacementController : MonoBehaviour
{    
    [SerializeField]
    private Button dismissButton;

    [SerializeField]
    private Button resetButton;

    [SerializeField]
    private GameObject welcomePanel;

    [SerializeField]
    private GameObject tooltipPanel;

    [SerializeField]
    private GameObject resetPanel;

    private ARRaycastManager arRaycastManager;

    private AddrPMBIndicator pmbIndicator;

    private AssetHandlerManager assetHandlerManager; 

    public bool isDetecting;

    public bool readyToLoad = false;
    
    void Awake() 
    {
        arRaycastManager = GetComponent<ARRaycastManager>();

        // set initial prefab
        // stallButton.onClick.AddListener(() => {readyToLoad = true;});
        dismissButton.onClick.AddListener(Dismiss);
        resetButton.onClick.AddListener(()=>{ 
            assetHandlerManager.DestroyRemotePrefab();
            resetPanel.SetActive(false);
            DetectionSwitch();
            tooltipPanel.SetActive(true);
            readyToLoad = false;             
        });
    }

    

    private void Dismiss() {
        welcomePanel.SetActive(false);
        tooltipPanel.SetActive(true);
    }

    public void DetectionSwitch(){
        if(isDetecting)
        {
            isDetecting = false;
            pmbIndicator.visual.SetActive(false); 
        }
        //Now used to enable detection after destroying object          
        else
        {
            isDetecting = true;
            pmbIndicator.visual.SetActive(true);        
        }
            
    }


    void Start()
    {
        pmbIndicator = FindObjectOfType<AddrPMBIndicator>();        
        assetHandlerManager = FindObjectOfType<AssetHandlerManager>();
        isDetecting = true;
        resetPanel.SetActive(false);
        tooltipPanel.SetActive(false);
    }

    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }    

    void Update()
    {
        if(welcomePanel.gameObject.activeSelf)
        {
            return;
        }

        if((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began) && isDetecting && !IsPointerOverUIObject())
        {
            // Debug.Log("Touch detected");
            
            if(pmbIndicator.visual.activeInHierarchy)
            {
                assetHandlerManager.LoadRemotePrefab();
                DetectionSwitch();
                tooltipPanel.SetActive(false);
                resetPanel.SetActive(true);
            }

            // if (onPlacedObject != null)
            // {
            //     onPlacedObject();
            // }
            // commented because it seems like it only shows indicator on touch, which is unnecessary
            // if(!pmbIndicator.visual.activeInHierarchy)
            //     pmbIndicator.visual.SetActive(true);
            
        }

       
    }
}

