﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CableDeconstruct : MonoBehaviour
{


    private PlacementControllerWithMultiple plCtrl;

    public int deconstructLevel = 0;
    
    void Awake() {
        
    }

    public void Deconstruct()
    {
        plCtrl.obj.transform.GetChild(1).gameObject.SetActive(false);
        if(deconstructLevel < plCtrl.obj.transform.GetChild(0).childCount-1)
        {
            plCtrl.obj.transform.GetChild(0).GetChild(deconstructLevel).gameObject.SetActive(false);
            deconstructLevel++;
        }
        else
        {
            Debug.LogError("Object cannot be deconstructed further!");
        }
    }

    public void Reconstruct()
    {
        plCtrl.obj.transform.GetChild(1).gameObject.SetActive(false);
        if(deconstructLevel > 0)
        {
            plCtrl.obj.transform.GetChild(0).GetChild(deconstructLevel).gameObject.SetActive(true);
            deconstructLevel--;
        }
        else if(deconstructLevel == 0)
        {
            plCtrl.obj.transform.GetChild(0).GetChild(deconstructLevel).gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Object cannot be reconstructed further!");
        }
        // for(int i=deconstructLevel; i>=0; i--)
        // {
        //     obj.transform.GetChild(0).GetChild(i).gameObject.SetActive(true);
        // }
    }

    public void StopDecRec()
    {
        // plCtrl.obj.transform.GetChild(1).gameObject.SetActive(true);
        plCtrl.labelActive = false;
        for(int i=deconstructLevel; i>=0; i--)
            Reconstruct();
        if(plCtrl.selectedPrefabName != "CableObject")
            plCtrl.deconstructFlag = false;
        plCtrl.deconstructPanel.SetActive(false);
    }


    // Start is called before the first frame update
    void Start()
    {
        plCtrl = FindObjectOfType<PlacementControllerWithMultiple>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
