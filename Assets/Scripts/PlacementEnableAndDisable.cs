﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARRaycastManager))]
public class PlacementEnableAndDisable : MonoBehaviour
{
    [SerializeField]
    private GameObject placedPrefab;

    
    [SerializeField]
    private Camera arCamera;

    

    private Vector2 touchPosition = default;

    private ARRaycastManager arRaycastManager;

    private bool onTouchHold = false;

    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    


    [SerializeField]
    private Button toggleButton;


    private ARPlaneManager aRPlaneManager;

    private GameObject PlacedPrefab 
    {
        get 
        {
            return placedPrefab;
        }
        set 
        {
            placedPrefab = value;
        }
    }


    void Awake() 
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
        aRPlaneManager = GetComponent<ARPlaneManager>();

        if(toggleButton != null)
        {
            toggleButton.onClick.AddListener(TogglePlaneDetection);
        }
    }

    private void TogglePlaneDetection()
    {
        aRPlaneManager.enabled = !aRPlaneManager.enabled;

        foreach(ARPlane plane in aRPlaneManager.trackables)
        {   
            plane.gameObject.SetActive(aRPlaneManager.enabled);
        }
        
        // toggleButton.GetComponentInChildren<Text>().text = aRPlaneManager.enabled ? 
        //     "Disable Plane Detection" : "Enable Plane Detection";

        toggleButton.GetComponentInChildren<Text>().text = aRPlaneManager.enabled ? 
            "Disable Plane Detection" : "Disable Plane Detection";
    }

    
   
}

