﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.EventSystems;


[RequireComponent(typeof(ARRaycastManager))]
public class PlacementControllerCableLabel : MonoBehaviour
{
    [SerializeField]
    private Button dismissButton;

    [SerializeField]
    private Button resetButton;

    [SerializeField]
    private Button labelTogglerButton;

    public ARSession session;

    [SerializeField]
    private GameObject welcomePanel;

    [SerializeField]
    private GameObject tooltipPanel;

    [SerializeField]
    private GameObject resetPanel;

    [SerializeField]
    private GameObject labelTogglerPanel;

    [SerializeField]
    private GameObject labelObject;

    [SerializeField]
    private GameObject placedPrefab;

    private GameObject placedPrefabWithLabels;

    private ARRaycastManager arRaycastManager;
    private PMBIndicatorCL pmbIndicatorCL;
    public bool isDetecting;

    private bool labelActive = true;

    GameObject obj;
    void Awake() 
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
        dismissButton.onClick.AddListener(Dismiss);
        resetButton.onClick.AddListener(()=>{ 
            //session.Reset();
            Destroy(obj);
            resetPanel.SetActive(false);
            labelTogglerPanel.SetActive(false);
            DetectionSwitch();
            tooltipPanel.SetActive(true);           
        });
        labelTogglerButton.onClick.AddListener(LabelToggle);
    }

    

    private void Dismiss() {
        welcomePanel.SetActive(false);
        tooltipPanel.SetActive(true);
    }

    private void LabelToggle() {
        if(labelActive)
        {
            labelActive = false;
            labelObject.SetActive(false);                
            Debug.LogWarning("Label Object deactivated");
        }
        else
        {
            labelActive = true;
            labelObject.SetActive(true);         
            Debug.LogWarning("Label Object activated");        
        }
    }

    

    public void DetectionSwitch(){
        if(isDetecting)
        {
            isDetecting = false;
            pmbIndicatorCL.visual.SetActive(false); 
        }
        //Now used to enable detection after destroying object          
        else
        {
            isDetecting = true;
            pmbIndicatorCL.visual.SetActive(true);        
        }
            
    }


    void Start()
    {
        pmbIndicatorCL = FindObjectOfType<PMBIndicatorCL>();
        isDetecting = true;
        resetPanel.SetActive(false);
        labelTogglerPanel.SetActive(false);
        tooltipPanel.SetActive(false);
    }

    

    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    

    void Update()
    {
        if(placedPrefab == null || welcomePanel.gameObject.activeSelf)
        {
            return;
        }

        if((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began) && isDetecting && !IsPointerOverUIObject())
        {
            obj = Instantiate(placedPrefab, pmbIndicatorCL.transform.position, pmbIndicatorCL.transform.rotation);
            DetectionSwitch();
            tooltipPanel.SetActive(false);
            resetPanel.SetActive(true);
            labelTogglerPanel.SetActive(true);
            // commented because it seems like it only shows indicator on touch, which is unnecessary
            // if(!pmbIndicatorCL.visual.activeInHierarchy)
            //     pmbIndicatorCL.visual.SetActive(true);
            
        }

       
    }


}
