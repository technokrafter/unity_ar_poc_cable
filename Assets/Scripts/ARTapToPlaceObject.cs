﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject objectToPlace;
    public GameObject placementIndicator;

    private ARRaycastManager rcManager;
    private Pose placementPose;
    private bool placementPoseIsValid = false;

    private bool isDetecting = false;

    private float baseAngle = 0.0f;

    // void OnMouseDown(){
    //    Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
    //    pos = Input.mousePosition - pos;
    //    baseAngle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
    //    baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) *Mathf.Rad2Deg;
    // }

    // void OnMouseDrag(){
    //     Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
    //     pos = Input.mousePosition - pos;
    //     float ang = Mathf.Atan2(pos.y, pos.x) *Mathf.Rad2Deg - baseAngle;
    //     transform.rotation = Quaternion.AngleAxis(ang, Vector3.forward);
    // }

    public void Start()
    {
        rcManager = FindObjectOfType<ARRaycastManager>();
        isDetecting = true;
    }

    public void DetectionOff(){
        isDetecting = false;
    }

    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    // public static bool IsPointerOverGameObject()
    //      {
    //          // Check mouse
    //          if (EventSystem.current.IsPointerOverGameObject())
    //          {
    //              return true;
    //          }
 
    //          // Check touches
    //          for (int i = 0; i < Input.touchCount; i++)
    //          {
    //              var touch = Input.GetTouch(i);
    //              if(touch.phase == TouchPhase.Began)
    //              {
    //                  if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
    //                  {
    //                      return true;
    //                  }
    //              }
    //          }
             
    //          return false;
    //      }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && isDetecting && !IsPointerOverUIObject())
        {
            PlaceObject();
        }
    }

    private void PlaceObject()
    {
        if(isDetecting)
        {
            Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
        }

        else
        {
            Debug.Log("PlaceObject deactivated");
        }
        
        
    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid && isDetecting && !IsPointerOverUIObject())
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()
    {
        if(isDetecting)
        {
            var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            var hits = new List<ARRaycastHit>();
            rcManager.Raycast(screenCenter, hits, TrackableType.Planes);

            placementPoseIsValid = hits.Count > 0;
            
            if (placementPoseIsValid)
            {
                placementPose = hits[0].pose;

                var cameraForward = Camera.main.transform.forward;
                var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
                placementPose.rotation = Quaternion.LookRotation(cameraBearing);
            }
        }
            
    }
}
